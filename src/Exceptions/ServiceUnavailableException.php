<?php

namespace Provisionesta\Google\Exceptions;

use Exception;

class ServiceUnavailableException extends Exception
{
    //
}
